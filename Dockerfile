FROM centos:centos6
MAINTAINER Stephen Mock <mock@tacc.utexas.edu>
RUN curl -O http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
RUN rpm -Uvh epel-release-6*.rpm
RUN yum -y update
RUN yum -y install git java-1.7.0-openjdk-devel.x86_64 java-1.7.0-openjdk.x86_64 postgresql-server.x86_64 nodejs.x86_64 npm.noarch
RUN curl -O https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
RUN chmod +x lein
RUN mv lein /usr/bin/
RUN useradd rodeo

#POSTGRES
RUN service postgresql initdb
ADD files/pg_hba.conf /var/lib/pgsql/data/pg_hba.conf
RUN /bin/chown postgres:postgres /var/lib/pgsql/data/pg_hba.conf
RUN ls -al /var/lib/pgsql/data/
RUN cat /var/lib/pgsql/data/pg_hba.conf
RUN chkconfig postgresql on 
ADD files/db_init.pgsql /db_init.pgsql
RUN chown postgres:postgres /db_init.pgsql
RUN service postgresql start; su - postgres -c '/usr/bin/psql -f /db_init.pgsql'; su - postgres -c 'pg_ctl stop'

#Back to root and the rest of the install
USER root
RUN /usr/bin/npm install -g bower

#as rodeo
USER rodeo
WORKDIR /home/rodeo
ENV HOME /home/rodeo
RUN /usr/bin/git clone https://github.com/alexkalderimis/staircase.git
USER root
ADD files/profiles.clj /home/rodeo/staircase/profiles.clj
ADD files/secrets.edn /home/rodeo/staircase/resources/secrets.edn
RUN chown rodeo:rodeo /home/rodeo/staircase/profiles.clj
RUN chown rodeo:rodeo /home/rodeo/staircase/resources/secrets.edn
USER rodeo
ENV HOME /home/rodeo
WORKDIR /home/rodeo/staircase
RUN /usr/bin/bower install -q
RUN curl -O https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
RUN chmod +x /home/rodeo/staircase/lein
RUN /home/rodeo/staircase/lein
RUN /home/rodeo/staircase/lein deps
USER root
#RUN service postgresql start; su - rodeo -c '/usr/bin/lein run'

